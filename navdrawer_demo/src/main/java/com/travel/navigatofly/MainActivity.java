package com.travel.navigatofly;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.LocaleList;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.travel.navigatofly.fragment.EmptyFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.aviasales.core.AviasalesSDK;
import ru.aviasales.core.identification.SdkConfig;
import ru.aviasales.core.search.params.Passengers;
import ru.aviasales.core.search.params.SearchParams;
import ru.aviasales.core.search.params.Segment;
import ru.aviasales.core.search_airports.object.PlaceData;
import ru.aviasales.template.ui.fragment.AviasalesFragment;
import ru.aviasales.template.ui.model.SearchFormData;
import ru.aviasales.template.ui.model.SimpleSearchParams;
import ru.aviasales.template.utils.Utils;

//import com.travel.navigatofly.fragment.MapViewFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private AviasalesFragment aviasalesFragment;

    // replace with your travel payout credentiials
    //	private final static String TRAVEL_PAYOUTS_MARKER = "your_travel_payouts_marker";
    private final static String TRAVEL_PAYOUTS_MARKER = "95940";
    //	private final static String TRAVEL_PAYOUTS_TOKEN = "your_travel_payouts_token";
    private final static String TRAVEL_PAYOUTS_TOKEN = "ea32a6f6a1a58c7e1d52681d574a527d";
    private final static String SDK_HOST = "www.travel-api.pw";
    private final static String APPODEAL_APP_KEY = "your_appodeal_app_key";

    private Dialog languageDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Uri data = getIntent().getData();

        if (data != null) {
            try{

                String origin_iata = data.getQueryParameter("origin_iata");
                String destination_iata = data.getQueryParameter("destination_iata");
                String depart_date = data.getQueryParameter("depart_date");
                String return_date = data.getQueryParameter("return_date");
                String adults = data.getQueryParameter("adults");
                String children = data.getQueryParameter("children");
                String infants = data.getQueryParameter("infants");
                String trip_class = data.getQueryParameter("trip_class");

                String dataParameters = data.toString().substring(data.toString().lastIndexOf("?") + 1);
                String[] uriTokens = dataParameters.split("&");
                for(int i=0;i<uriTokens.length;i++){
                    String[] valTokens = uriTokens[i].split("=");
                    switch(valTokens[0]){
                        case "origin_iata":
                            origin_iata = valTokens[1];
                            break;
                        case "destination_iata":
                            destination_iata = valTokens[1];
                            break;
                        case "depart_date":
                            depart_date = valTokens[1];
                            break;
                        case "return_date":
                            return_date = valTokens[1];
                            break;
                        case "adults":
                            adults = valTokens[1];
                            break;
                        case "children":
                            children = valTokens[1];
                            break;
                        case "infants":
                            infants = valTokens[1];
                            break;
                        case "trip_class":
                            trip_class = valTokens[1];
                            break;
                    }
                }

                SharedPreferences prefs = Utils.getPreferences(this);

                SearchFormData searchFormData = new SearchFormData(this);
                searchFormData.setPassengers(createPassengers(prefs, adults, children, infants));
                searchFormData.setTripClass(createTripClass(prefs, trip_class));
                SimpleSearchParams simpleSearchParams = new SimpleSearchParams();
                PlaceData origin = new PlaceData();
                origin.setIata(origin_iata);
                simpleSearchParams.setOrigin(origin);
                PlaceData destination = new PlaceData();
                destination.setIata(destination_iata);
                simpleSearchParams.setOrigin(destination);
                simpleSearchParams.setDepartDate(depart_date);
                simpleSearchParams.setReturnDate(return_date);
                simpleSearchParams.setReturnEnabled(!return_date.isEmpty());
                searchFormData.setSimpleSearchParams(simpleSearchParams);

                AviasalesSDK.getInstance().init(this, new SdkConfig(TRAVEL_PAYOUTS_MARKER, TRAVEL_PAYOUTS_TOKEN, SDK_HOST));
                showAviasalesSdkFragment(searchFormData);
            }catch (Exception e)
            {
                e.printStackTrace();
                init(savedInstanceState);
            }
        }else
            init(savedInstanceState);

//		createBanner();
    }

    private void init(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            AviasalesSDK.getInstance().init(this, new SdkConfig(TRAVEL_PAYOUTS_MARKER, TRAVEL_PAYOUTS_TOKEN, SDK_HOST));
            showAviasalesSdkFragment(null);
        }
    }

    private void showAviasalesSdkFragment(SearchFormData searchFormData) {
        FragmentManager fm = getSupportFragmentManager();

        aviasalesFragment = (AviasalesFragment) fm.findFragmentByTag(AviasalesFragment.TAG);
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        if (aviasalesFragment == null) {
            if(searchFormData == null)
                aviasalesFragment = (AviasalesFragment) AviasalesFragment.newInstance();
            else
                aviasalesFragment = (AviasalesFragment) AviasalesFragment.newInstance(searchFormData);
        }
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        fragmentTransaction.replace(R.id.content_main, aviasalesFragment, AviasalesFragment.TAG);
        fragmentTransaction.commit();
    }

    private Passengers createPassengers(SharedPreferences prefs, String adults, String children, String infants) {
        Passengers passengers = new Passengers();
        try{
            passengers.setAdults(Integer.parseInt(adults));
        }catch (NumberFormatException e) {
            passengers.setAdults(prefs.getInt(SearchParams.SEARCH_PARAM_ADULTS, 1));
        }
        try{
            passengers.setChildren(Integer.parseInt(children));
        }catch (NumberFormatException e) {
            passengers.setChildren(prefs.getInt(SearchParams.SEARCH_PARAM_ADULTS, 0));
        }
        try{
            passengers.setInfants(Integer.parseInt(infants));
        }catch (NumberFormatException e) {
            passengers.setInfants(prefs.getInt(SearchParams.SEARCH_PARAM_ADULTS, 0));
        }
        return passengers;
    }

    private String createTripClass(SharedPreferences prefs, String trip_class) {
        try {
            return SearchParams.convertToNewTripClass(Integer.parseInt(trip_class));
        } catch (NumberFormatException e) {
            return prefs.getString(SearchParams.SEARCH_PARAM_TRIP_CLASS, SearchParams.TRIP_CLASS_ECONOMY);
        }
    }

    public List<Segment> createSegments(String origin_iata, String destination_iata, String depart_date, String return_date) {
        List<Segment> segments = new ArrayList<>();
        Segment departSegment = new Segment();
        departSegment.setOrigin(origin_iata);
        departSegment.setDestination(destination_iata);
        departSegment.setDate(depart_date);

        segments.add(departSegment);

        if (!return_date.isEmpty()) {
            Segment returnSegment = new Segment();
            returnSegment.setOrigin(destination_iata);
            returnSegment.setDestination(origin_iata);
            returnSegment.setDate(return_date);

            segments.add(returnSegment);
        }

        return segments;
    }

    private void showEmptyFragment() {
        FragmentManager fm = getSupportFragmentManager();

        EmptyFragment emptyFragment = (EmptyFragment) fm.findFragmentByTag(EmptyFragment.TAG);

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        if (emptyFragment == null) {
            emptyFragment = new EmptyFragment();
            fragmentTransaction.addToBackStack(null);

        }
        fragmentTransaction.replace(R.id.content_main, emptyFragment, EmptyFragment.TAG);
        fragmentTransaction.commit();
    }

    private void showMapFragment() {
//		SearchFormData searchFormData = aviasalesFragment.getSearchFormData();
//		SimpleSearchParams searchParams = searchFormData.getSimpleSearchParams();
//
//		FragmentManager fm = getSupportFragmentManager();
//
//		MapViewFragment mapFragment = (MapViewFragment) fm.findFragmentByTag(MapViewFragment.TAG);
//		MyApplication.originData = searchParams;
//
//		FragmentTransaction fragmentTransaction = fm.beginTransaction();
//
//		if (mapFragment == null) {
//			mapFragment = new MapViewFragment();
//			fragmentTransaction.addToBackStack(null);
//
//		}
//		fragmentTransaction.replace(R.id.content_main, mapFragment, MapViewFragment.TAG);
////		fragmentTransaction.commit();
//		fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (!(getAviasalesFragment() != null && getAviasalesFragment().onBackPressed())) {
                super.onBackPressed();
            }
        }
    }

    @Nullable
    private AviasalesFragment getAviasalesFragment() {
        if (aviasalesFragment == null) {
            aviasalesFragment = (AviasalesFragment) getSupportFragmentManager().findFragmentByTag(AviasalesFragment.TAG);
            if (aviasalesFragment != null) {
                return aviasalesFragment;
            } else {
                return null;
            }
        } else {
            return aviasalesFragment;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_search) {
//			showAviasalesSdkFragment();
            aviasalesFragment.toSearchFormFragment(false);
        } else if (id == R.id.nav_multi_city) {
//			showAviasalesSdkFragment();
            aviasalesFragment.toSearchFormFragment(true);
//		} else if (id == R.id.nav_price_map) {
//			showMapFragment();
        } else if (id == R.id.nav_language) {
            showLanguageDialog();
        } else if (id == R.id.nav_currency) {
            aviasalesFragment.createCurrencyDialog();
        } else if (id == R.id.nav_share) {
            inviteOthers();
		} else if (id == R.id.nav_rate) {
            openAppRating(this);
        } else if (id == R.id.nav_about) {
//			showEmptyFragment();
            aviasalesFragment.showAboutFragment();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public static void openAppRating(Context context) {
        // you can also use BuildConfig.APPLICATION_ID
        String appId = "com.travel.navigatorfly";
        Intent rateIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + appId));
        boolean marketFound = false;

        // find all applications able to handle our rateIntent
        final List<ResolveInfo> otherApps = context.getPackageManager()
                .queryIntentActivities(rateIntent, 0);
        for (ResolveInfo otherApp: otherApps) {
            // look for Google Play application
            if (otherApp.activityInfo.applicationInfo.packageName
                    .equals("com.android.vending")) {

                ActivityInfo otherAppActivity = otherApp.activityInfo;
                ComponentName componentName = new ComponentName(
                        otherAppActivity.applicationInfo.packageName,
                        otherAppActivity.name
                );
                // make sure it does NOT open in the stack of your activity
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // task reparenting if needed
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                // if the Google Play was already open in a search result
                //  this make sure it still go to the app page you requested
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // this make sure only the Google Play app is allowed to
                // intercept the intent
                rateIntent.setComponent(componentName);
                context.startActivity(rateIntent);
                marketFound = true;
                break;

            }
        }

        // if GP not present on device, open web browser
        if (!marketFound) {
            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id="+appId));
            context.startActivity(webIntent);
        }
    }

    public static String language_code = "en";

    private void showLanguageDialog() {
        languageDialog = new Dialog(MainActivity.this);
//		languageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        languageDialog.setTitle("Language");
        languageDialog.setContentView(R.layout.language_dialog);

        setRadioLanguage();

        Button btn_ok = (Button) languageDialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().changeLocale(language_code);
                aviasalesFragment.setLocale(language_code);
                languageDialog.dismiss();
            }
        });

        languageDialog.show();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // refresh your views here
        super.onConfigurationChanged(newConfig);
    }

    private void setRadioLanguage() {
        RadioButton radio_en = (RadioButton) languageDialog.findViewById(R.id.radio_en);
        radio_en.setOnClickListener(this);
        RadioButton radio_ar = (RadioButton) languageDialog.findViewById(R.id.radio_ar);
        radio_ar.setOnClickListener(this);
        RadioButton radio_az = (RadioButton) languageDialog.findViewById(R.id.radio_az);
        radio_az.setOnClickListener(this);
        RadioButton radio_be = (RadioButton) languageDialog.findViewById(R.id.radio_be);
        radio_be.setOnClickListener(this);
        RadioButton radio_fr = (RadioButton) languageDialog.findViewById(R.id.radio_fr);
        radio_fr.setOnClickListener(this);
        RadioButton radio_hy = (RadioButton) languageDialog.findViewById(R.id.radio_hy);
        radio_hy.setOnClickListener(this);
        RadioButton radio_ka = (RadioButton) languageDialog.findViewById(R.id.radio_ka);
        radio_ka.setOnClickListener(this);
        RadioButton radio_kk = (RadioButton) languageDialog.findViewById(R.id.radio_kk);
        radio_kk.setOnClickListener(this);
        RadioButton radio_ky = (RadioButton) languageDialog.findViewById(R.id.radio_ky);
        radio_ky.setOnClickListener(this);
        RadioButton radio_ro = (RadioButton) languageDialog.findViewById(R.id.radio_ro);
        radio_ro.setOnClickListener(this);
        RadioButton radio_ru = (RadioButton) languageDialog.findViewById(R.id.radio_ru);
        radio_ru.setOnClickListener(this);
        RadioButton radio_uk = (RadioButton) languageDialog.findViewById(R.id.radio_uk);
        radio_uk.setOnClickListener(this);
        RadioButton radio_uz = (RadioButton) languageDialog.findViewById(R.id.radio_uz);
        radio_uz.setOnClickListener(this);
        RadioButton radio_pl = (RadioButton) languageDialog.findViewById(R.id.radio_pl);
        radio_pl.setOnClickListener(this);

        if (language_code.equals(getResources().getString(R.string.en))) {
            radio_en.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.ar))) {
            radio_ar.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.az))) {
            radio_az.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.be))) {
            radio_be.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.fr))) {
            radio_fr.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.hy))) {
            radio_hy.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.ka))) {
            radio_ka.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.kk))) {
            radio_kk.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.ky))) {
            radio_ky.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.ro))) {
            radio_ro.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.ru))) {
            radio_ru.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.uk))) {
            radio_uk.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.uz))) {
            radio_uz.setChecked(true);
        } else if (language_code.equals(getResources().getString(R.string.pl))) {
            radio_uz.setChecked(true);
        }

    }

    private void inviteOthers() {

        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            String strShareMessage = "\nLet me recommend you this application\n\n";
            strShareMessage = strShareMessage + "https://play.google.com/store/apps/details?id="
                    + getPackageName();
            Uri screenshotUri = Uri.parse("android.resource://packagename/drawable/image_name");
            i.setType("image/png");
            i.putExtra(Intent.EXTRA_STREAM, screenshotUri);
            i.putExtra(Intent.EXTRA_TEXT, strShareMessage);
            startActivity(Intent.createChooser(i, "Share via"));
        } catch (Exception e) {
            //e.toString();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.radio_en:
                language_code = getResources().getString(R.string.en);
                break;
            case R.id.radio_ar:
                language_code = getResources().getString(R.string.ar);
                break;
            case R.id.radio_az:
                language_code = getResources().getString(R.string.az);
                break;
            case R.id.radio_be:
                language_code = getResources().getString(R.string.be);
                break;
            case R.id.radio_fr:
                language_code = getResources().getString(R.string.fr);
                break;
            case R.id.radio_hy:
                language_code = getResources().getString(R.string.hy);
                break;
            case R.id.radio_ka:
                language_code = getResources().getString(R.string.ka);
                break;
            case R.id.radio_kk:
                language_code = getResources().getString(R.string.kk);
                break;
            case R.id.radio_ky:
                language_code = getResources().getString(R.string.ky);
                break;
            case R.id.radio_ro:
                language_code = getResources().getString(R.string.ro);
                break;
            case R.id.radio_ru:
                language_code = getResources().getString(R.string.ru);
                break;
            case R.id.radio_uk:
                language_code = getResources().getString(R.string.uk);
                break;
            case R.id.radio_uz:
                language_code = getResources().getString(R.string.uz);
                break;
            case R.id.radio_pl:
                language_code = getResources().getString(R.string.pl);
                break;
        }
    }

}
