package com.travel.navigatofly.model;

/**
 * Created by jon on 1/4/2017.
 */

public class Weather {

    private String weathertype;
    private String temp_min;
    private String temp_max;

    public Weather() {
    }

    public String getWeathertype() {
        return weathertype;
    }

    public void setWeathertype(String weathertype) {
        this.weathertype = weathertype;
    }

    public String getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(String temp_min) {
        this.temp_min = temp_min;
    }

    public String getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(String temp_max) {
        this.temp_max = temp_max;
    }
}
