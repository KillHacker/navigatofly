package com.travel.navigatofly.model;

/**
 * Created by jon on 1/4/2017.
 */

public class Coordinate {

    private double lon = 0;
    private double lat = 0;

    public Coordinate() {
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
