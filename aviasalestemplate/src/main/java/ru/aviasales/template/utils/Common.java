package ru.aviasales.template.utils;

import android.support.v7.widget.RecyclerView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ru.aviasales.core.search.object.Proposal;
import ru.aviasales.template.ui.adapter.AdAdapter;
import ru.aviasales.template.ui.adapter.ResultsRecycleViewAdapter;

/**
 * Created by Quan on 11/14/2017.
 */

public class Common {

    public final static String TRAVEL_PAYOUTS_MARKER = "95940";
    public final static String TRAVEL_PAYOUTS_TOKEN = "ea32a6f6a1a58c7e1d52681d574a527d";
    public final static String SDK_HOST = "www.travel-api.pw";

    public static ResultsRecycleViewAdapter resultsAdapter;
    public static RecyclerView resultsListView;
    public static RecyclerView resultsListView_Direct;

//    public static ListView resultsListView;
    public static AdAdapter adAdapter;

    public static List<Proposal> filteredProposals_all  = new ArrayList<>();
    public static List<Proposal> filteredProposals_direct  = new ArrayList<>();
    public static List<Proposal> filteredProposals_best  = new ArrayList<>();

    public static boolean isFirstPageFragment = true;
}
