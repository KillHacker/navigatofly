package ru.aviasales.template.ui.fragment;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ru.aviasales.adsinterface.AdsInterface;
import ru.aviasales.core.AviasalesSDK;
import ru.aviasales.core.ads.AdsManager;
import ru.aviasales.core.search.object.Proposal;
import ru.aviasales.core.search.object.SearchData;
import ru.aviasales.core.search.params.SearchParams;
import ru.aviasales.template.R;
import ru.aviasales.template.ads.AdsImplKeeper;
import ru.aviasales.template.currencies.Currency;
import ru.aviasales.template.filters.manager.FiltersManager;
import ru.aviasales.template.proposal.ProposalManager;
import ru.aviasales.template.ui.adapter.AdAdapter;
import ru.aviasales.template.ui.adapter.ResultsRecycleViewAdapter;
import ru.aviasales.template.ui.adapter.TabsFragmentPagerAdapter;
import ru.aviasales.template.ui.dialog.CurrencyFragmentDialog;
import ru.aviasales.template.ui.dialog.ResultsSortingDialog;
import ru.aviasales.template.utils.BrowserUtils;
import ru.aviasales.template.utils.Common;
import ru.aviasales.template.utils.CurrencyUtils;
import ru.aviasales.template.utils.SortUtils;
import ru.aviasales.template.utils.StringUtils;

public class ResultsFragment extends BaseFragment {

	public static int intSelectedTab = -1;
	private static int resultsCount = -1;

	private ResultsRecycleViewAdapter resultsAdapter;
	private AdAdapter adAdapter;

	private View rootView;
	private RecyclerView resultsListView;
	private TextView currencyTextView;

	public static ResultsFragment newInstance() {
		return new ResultsFragment();
	}

	@Override
	protected void resumeDialog(String removedDialogFragmentTag) {
		if (removedDialogFragmentTag.equals(ResultsSortingDialog.TAG)) {
			createSortingDialog();
		}

		if (removedDialogFragmentTag.equals(CurrencyFragmentDialog.TAG)) {
			createCurrencyDialog();
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		Common.filteredProposals_all.clear();
		Common.filteredProposals_direct.clear();
		Common.filteredProposals_best.clear();Common.filteredProposals_all = FiltersManager.getInstance().getFilteredProposals();
		for(Proposal item:Common.filteredProposals_all){
			if(item.isDirect()){
				Common.filteredProposals_direct.add(item);
			}
			if(item.isTrusted()){
				Common.filteredProposals_best.add(item);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.results_fragment, container, false);
//		setUpViews();
//		setupActionBarCustomView();

		createTabLayout();

		return rootView;
	}

	public static ResultsRecycleViewAdapter proposalsAdapter_direct;
	public static ResultsRecycleViewAdapter proposalsAdapter_best;

	private void createTabLayout(){


		TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);

//		tabLayout.setSele

		tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){
			@Override
			public void onTabSelected(TabLayout.Tab tab){
				int position = tab.getPosition();
				intSelectedTab = position;
				setTabPageFragment();
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {
				int position = tab.getPosition();

			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {
				int position = tab.getPosition();

			}
		});

		ViewPager pager;
		pager = (ViewPager) rootView.findViewById(R.id.pager);
		TabsFragmentPagerAdapter adapter = new TabsFragmentPagerAdapter(getChildFragmentManager(), getContext());


		if(pager!=null && adapter!=null) {
			pager.setAdapter(adapter);
			adapter.notifyDataSetChanged();
			tabLayout.setupWithViewPager(pager);
		}






	}

	private void setTabPageFragment(){

		if(Common.resultsListView!=null) {
			switch (intSelectedTab) {
				case 0:
					setAllTicket();
					break;
				case 1:
					setDirect();
					break;
				case 2:
					setBest();
					break;
			}
		}
	}

	public void setAllTicket(){
		if(Common.resultsListView!=null) {
			ResultsRecycleViewAdapter proposalsAdapter_all = createOrRefreshAdapter();
			Common.adAdapter = createAdAdapter(proposalsAdapter_all);
			proposalsAdapter_all.setListener(new ResultsRecycleViewAdapter.OnClickListener() {
				@Override
				public void onClick(final Proposal proposal, int position) {
					if (getActivity() == null) return;
					showDetails(proposal);
				}
			});
			proposalsAdapter_all.sortProposals(SortUtils.getSavedSortingType());
			Common.resultsListView.setAdapter(Common.adAdapter);
			Common.adAdapter.notifyDataSetChanged();
		}
	}

	public void setDirect(){
		if(Common.resultsListView!=null) {
			boolean isComplexSearch = AviasalesSDK.getInstance().getSearchData().isComplexSearch();
			proposalsAdapter_direct = new ResultsRecycleViewAdapter(getActivity(), Common.filteredProposals_direct, isComplexSearch);
			proposalsAdapter_direct.reloadFilteredTickets(Common.filteredProposals_direct, SortUtils.getSavedSortingType());

//							ResultsRecycleViewAdapter proposalsAdapter_direct = createOrRefreshAdapter();
			proposalsAdapter_direct.setListener(new ResultsRecycleViewAdapter.OnClickListener() {
				@Override
				public void onClick(final Proposal proposal, int position) {
					if (getActivity() == null) return;
					showDetails(proposal);
				}
			});
			proposalsAdapter_direct.sortProposals(SortUtils.getSavedSortingType());

			Common.resultsListView.setAdapter(proposalsAdapter_direct);
			proposalsAdapter_direct.notifyDataSetChanged();
		}
	}

	public void setBest(){
		if(Common.resultsListView!=null) {
			boolean isComplexSearch = AviasalesSDK.getInstance().getSearchData().isComplexSearch();
			proposalsAdapter_best = new ResultsRecycleViewAdapter(getActivity(), Common.filteredProposals_best, isComplexSearch);
			proposalsAdapter_best.reloadFilteredTickets(Common.filteredProposals_best, SortUtils.getSavedSortingType());

//							ResultsRecycleViewAdapter proposalsAdapter_direct = createOrRefreshAdapter();
			proposalsAdapter_best.setListener(new ResultsRecycleViewAdapter.OnClickListener() {
				@Override
				public void onClick(final Proposal proposal, int position) {
					if (getActivity() == null) return;
					showDetails(proposal);
				}
			});
			proposalsAdapter_best.sortProposals(SortUtils.getSavedSortingType());

			Common.resultsListView.setAdapter(proposalsAdapter_best);
			proposalsAdapter_best.notifyDataSetChanged();
		}
	}

	@Override
	public void onDestroyView() {
		resultsAdapter = null;
		adAdapter = null;
		super.onDestroyView();
	}

	private void setupActionBarCustomView() {
		showActionBar(true);
		setTextToActionBar(StringUtils.getFirstAndLastIatasString(getSearchParams()));
	}

	private void setUpViews() {

		resultsListView = (RecyclerView) rootView.findViewById(R.id.lv_results);
		setUpListView(resultsListView);
		resultsListView.setHasFixedSize(true);

		resultsListView.setLayoutManager(new LinearLayoutManager(getActivity()));

	}

	private void setUpListView(RecyclerView listView) {

		final ResultsRecycleViewAdapter proposalsAdapter = createOrRefreshAdapter();
		adAdapter = createAdAdapter(proposalsAdapter);
		listView.setAdapter(adAdapter);

		proposalsAdapter.setListener(new ResultsRecycleViewAdapter.OnClickListener() {
			@Override
			public void onClick(final Proposal proposal, int position) {
				if (getActivity() == null) return;

				showDetails(proposal);
			}
		});
		proposalsAdapter.sortProposals(SortUtils.getSavedSortingType());
	}

	private AdAdapter createAdAdapter(ResultsRecycleViewAdapter adapter) {
		AdAdapter adAdapter = new AdAdapter(adapter, new AdsManager.AdListener() {
			@Override
			public void onAdBannerPressed() {
				AdsManager instance = AdsManager.getInstance();
				if (instance.useInternalBrowser()) {
					BrowserUtils.openInternalBrowser(getActivity(), instance.getFullAdsUrl(), instance.getResultsAdsBrowserTitle(), null, false);
				} else {
					BrowserUtils.openExternalBrowser(getActivity(), instance.getFullAdsUrl(), null);
				}
			}
		});

		AdsInterface adsInterface = AdsImplKeeper.getInstance().getAdsInterface();
		adAdapter.setShouldShowAppodealAdBanner(adsInterface.isResultsAdsEnabled() && adsInterface.areResultsReadyToShow());
		AdsManager adsManager = AdsManager.getInstance();
		adAdapter.setShouldShowAsBanner(adsManager.needToShowAdsOnResults() && adsManager.isWebViewLoaded());
		return adAdapter;
	}

	private ResultsRecycleViewAdapter createOrRefreshAdapter() {
		ResultsRecycleViewAdapter adapter;

//		List<Proposal> filteredProposals_all = FiltersManager.getInstance().getFilteredProposals();
		if (Common.resultsAdapter == null) {
			boolean isComplexSearch = AviasalesSDK.getInstance().getSearchData().isComplexSearch();
			if(intSelectedTab==0) {
				Common.resultsAdapter = new ResultsRecycleViewAdapter(getActivity(), Common.filteredProposals_all, isComplexSearch);
			} else if(intSelectedTab==1){
				Common.resultsAdapter = new ResultsRecycleViewAdapter(getActivity(), Common.filteredProposals_direct, isComplexSearch);
			}
		} else {
			if(intSelectedTab==0) {
				Common.resultsAdapter.reloadFilteredTickets(Common.filteredProposals_all, SortUtils.getSavedSortingType());
			} else if(intSelectedTab==1){
				Common.resultsAdapter.reloadFilteredTickets(Common.filteredProposals_direct, SortUtils.getSavedSortingType());
			}
		}
		adapter = Common.resultsAdapter;

		return adapter;
	}

	private SearchData getSearchResults() {
		return AviasalesSDK.getInstance().getSearchData();
	}

	public void showDetails(Proposal ticketData) {
		ProposalManager.getInstance().init(ticketData, AviasalesSDK.getInstance().getSearchData().getGatesInfo(),
				AviasalesSDK.getInstance().getSearchParamsOfLastSearch());
		startFragment(TicketDetailsFragment.newInstance(), true);
	}

	private void checkAppDataAvailability() {
		if (getActivity() == null || getSearchResults() == null || getSearchParams() == null) {
			Toast.makeText(getActivity(), getResources().getString(R.string.toast_no_search_results), Toast.LENGTH_SHORT).show();
			getActivity().onBackPressed();
		} else {
			if (resultsAdapter != null) {
				resultsAdapter.notifyDataSetChanged();
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.results_menu, menu);
		View currency = null;

		if (Build.VERSION.SDK_INT <= 10) {
			currency = MenuItemCompat.getActionView(menu.findItem(R.id.currency));
		} else {
			currency = menu.findItem(R.id.currency).getActionView();
		}

		AviasalesFragment.currencyTextView = (TextView) currency.findViewById(R.id.tv_currency);

		updateCurrencyTextView();

		currency.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				createCurrencyDialog();
			}
		});

		super.onCreateOptionsMenu(menu, inflater);
	}

	private void updateCurrencyTextView() {
		AviasalesFragment.currencyTextView.setText(StringUtils.getSpannableString(CurrencyUtils.getAppCurrency(getActivity()), new UnderlineSpan()));
	}

	private void createCurrencyDialog() {

		CurrencyFragmentDialog dialog = CurrencyFragmentDialog.newInstance(new CurrencyFragmentDialog.OnCurrencyChangedListener() {
			@Override
			public void onCurrencyChanged(String code) {
				CurrencyUtils.setAppCurrency(code, getActivity());
				updateCurrencyTextView();
				if(intSelectedTab==0 && Common.resultsAdapter!=null)	{
					Common.resultsAdapter.notifyDataSetChanged();
					Common.resultsListView.setAdapter(Common.resultsAdapter);
				}
				if(intSelectedTab==1 && proposalsAdapter_direct!=null) {
					proposalsAdapter_direct.notifyDataSetChanged();
					Common.resultsListView.setAdapter(proposalsAdapter_direct);
				}
				if(intSelectedTab==2 && proposalsAdapter_best!=null) {
					proposalsAdapter_best.notifyDataSetChanged();
					Common.resultsListView.setAdapter(proposalsAdapter_best);
				}
				dismissDialog();
			}

			@Override
			public void onCancel() {
				dismissDialog();
			}
		});
		List<Currency> currencies = CurrencyUtils.getCurrenciesList();
		dialog.setItems(currencies);
		createDialog(dialog);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.sort) {
			if (Common.resultsAdapter != null && Common.resultsListView != null) {
				createSortingDialog();
			}
			return true;
		} else if (id == R.id.filters) {
			startFragment(FiltersFragment.newInstance(), true);
			return true;
		} else if (id == R.id.currency) {
//			startFragment(FiltersFragment.newInstance(), true);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	private void createSortingDialog() {
		createDialog(ResultsSortingDialog.newInstance(SortUtils.getSavedSortingType(),
				AviasalesSDK.getInstance().getSearchParamsOfLastSearch().isComplexSearch(), new ResultsSortingDialog.OnSortingChangedListener() {
					@Override
					public void onSortingChanged(int sortingType) {

						switch (intSelectedTab){
							case 0:
								Common.resultsListView.setAdapter(Common.adAdapter);
								Common.resultsAdapter.sortProposals(sortingType);
								break;
							case 1:
								Common.resultsListView.setAdapter(proposalsAdapter_direct);
								proposalsAdapter_direct.sortProposals(sortingType);
								break;
							case 2:
								Common.resultsListView.setAdapter(proposalsAdapter_best);
								proposalsAdapter_best.sortProposals(sortingType);
								break;
						}

						dismissDialog();
					}

					@Override
					public void onCancel() {
						dismissDialog();
					}
				}));
	}

	@Override
	public void onResume() {
		super.onResume();
		checkAppDataAvailability();

		if (resultsCount != -1 && resultsCount != resultsAdapter.getItemCount()) {
			resultsListView.scrollToPosition(0);
			resultsCount = -1;
		}

	}

	protected SearchParams getSearchParams() {
		return AviasalesSDK.getInstance().getSearchParamsOfLastSearch();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onPause() {
		super.onPause();

		if (resultsAdapter != null) {
			resultsCount = resultsAdapter.getItemCount();
		}
	}
}
