package ru.aviasales.template.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import ru.aviasales.adsinterface.AdsInterface;
import ru.aviasales.core.AviasalesSDK;
import ru.aviasales.core.ads.AdsManager;
import ru.aviasales.core.search.object.Proposal;
import ru.aviasales.core.search.object.SearchData;
import ru.aviasales.core.search.params.SearchParams;
import ru.aviasales.template.R;
import ru.aviasales.template.ads.AdsImplKeeper;
import ru.aviasales.template.proposal.ProposalManager;
import ru.aviasales.template.ui.adapter.AdAdapter;
import ru.aviasales.template.ui.adapter.ResultsRecycleViewAdapter;
import ru.aviasales.template.utils.BrowserUtils;
import ru.aviasales.template.utils.Common;
import ru.aviasales.template.utils.SortUtils;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PageFragment_Direct.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PageFragment_Direct#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PageFragment_Direct extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;  //page number
    private String mParam2;

    private AviasalesFragment aviasalesFragment;
    private static final String TAG = PageFragment_Direct.class.getSimpleName();

    private OnFragmentInteractionListener mListener;


    private static int resultsCount = -1;

    private ResultsRecycleViewAdapter resultsAdapter;
    private AdAdapter adAdapter;

    private View rootView;
    private RecyclerView resultsListView;
    private TextView currencyTextView;

    public PageFragment_Direct() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PageFragment_Direct newInstance(int param1, String param2) { //param1 is page number
        PageFragment_Direct fragment = new PageFragment_Direct();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_page, container, false);

        initialUI();
        return rootView;
    }


    private void initialUI(){

        switch (mParam1) {
            case 0:

                break;
            case 1:
                initFragment();
                break;
            case 2:
                break;
        }

    }

    private void initFragment() {
        setUpViews();
//        setupActionBarCustomView();
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void setUpViews() {

        Common.resultsListView_Direct = (RecyclerView) rootView.findViewById(R.id.lv_results1);

        if(Common.isFirstPageFragment) {
            setUpListView(Common.resultsListView_Direct);
//            Common.isFirstPageFragment = false;
        }

        Common.resultsListView_Direct.setHasFixedSize(true);

        Common.resultsListView_Direct.setLayoutManager(new LinearLayoutManager(getActivity()));

    }

//    private void setUpListView(RecyclerView listView) {
    private void setUpListView(RecyclerView listView) {

        ResultsRecycleViewAdapter proposalsAdapter_all;
        ResultsRecycleViewAdapter proposalsAdapter_direct;
        ResultsRecycleViewAdapter proposalsAdapter_best;

        if(mParam1==0) {
            proposalsAdapter_all = createOrRefreshAdapter();
            Common.adAdapter = createAdAdapter(proposalsAdapter_all);
            proposalsAdapter_all.setListener(new ResultsRecycleViewAdapter.OnClickListener() {
                @Override
                public void onClick(final Proposal proposal, int position) {
                    if (getActivity() == null) return;
                    showDetails(proposal);
                }
            });
            proposalsAdapter_all.sortProposals(SortUtils.getSavedSortingType());
            listView.setAdapter(Common.adAdapter);
        }else if(mParam1==1){
            boolean isComplexSearch = AviasalesSDK.getInstance().getSearchData().isComplexSearch();
            proposalsAdapter_direct = new ResultsRecycleViewAdapter(getActivity(), Common.filteredProposals_direct, isComplexSearch);
            proposalsAdapter_direct.reloadFilteredTickets(Common.filteredProposals_direct, SortUtils.getSavedSortingType());

            listView.setAdapter(proposalsAdapter_direct);

//            Common.adAdapter = createAdAdapter(proposalsAdapter_direct);
            proposalsAdapter_direct.setListener(new ResultsRecycleViewAdapter.OnClickListener() {
                @Override
                public void onClick(final Proposal proposal, int position) {
                    if (getActivity() == null) return;
                    showDetails(proposal);
                }
            });
            proposalsAdapter_direct.sortProposals(SortUtils.getSavedSortingType());

        }else if(mParam1==2){
            boolean isComplexSearch = AviasalesSDK.getInstance().getSearchData().isComplexSearch();
            proposalsAdapter_best = new ResultsRecycleViewAdapter(getActivity(), Common.filteredProposals_best, isComplexSearch);
            proposalsAdapter_best.reloadFilteredTickets(Common.filteredProposals_best, SortUtils.getSavedSortingType());

            listView.setAdapter(proposalsAdapter_best);

//            Common.adAdapter = createAdAdapter(proposalsAdapter_direct);
            proposalsAdapter_best.setListener(new ResultsRecycleViewAdapter.OnClickListener() {
                @Override
                public void onClick(final Proposal proposal, int position) {
                    if (getActivity() == null) return;
                    showDetails(proposal);
                }
            });
            proposalsAdapter_best.sortProposals(SortUtils.getSavedSortingType());

        }


    }

    private AdAdapter createAdAdapter(ResultsRecycleViewAdapter adapter) {
        AdAdapter adAdapter = new AdAdapter(adapter, new AdsManager.AdListener() {
            @Override
            public void onAdBannerPressed() {
                AdsManager instance = AdsManager.getInstance();
                if (instance.useInternalBrowser()) {
                    BrowserUtils.openInternalBrowser(getActivity(), instance.getFullAdsUrl(), instance.getResultsAdsBrowserTitle(), null, false);
                } else {
                    BrowserUtils.openExternalBrowser(getActivity(), instance.getFullAdsUrl(), null);
                }
            }
        });
//        if(mParam1==0) {
            AdsInterface adsInterface = AdsImplKeeper.getInstance().getAdsInterface();
            adAdapter.setShouldShowAppodealAdBanner(adsInterface.isResultsAdsEnabled() && adsInterface.areResultsReadyToShow());
            AdsManager adsManager = AdsManager.getInstance();
            adAdapter.setShouldShowAsBanner(adsManager.needToShowAdsOnResults() && adsManager.isWebViewLoaded());
//        }
        return adAdapter;
    }


    private ResultsRecycleViewAdapter createOrRefreshAdapter() {
        ResultsRecycleViewAdapter adapter;

//        List<Proposal> filteredProposals = FiltersManager.getInstance().getFilteredProposals();

        if (Common.resultsAdapter == null) {
            boolean isComplexSearch = AviasalesSDK.getInstance().getSearchData().isComplexSearch();
            if(mParam1==0) {
                Common.resultsAdapter = new ResultsRecycleViewAdapter(getActivity(), Common.filteredProposals_all, isComplexSearch);
            } else if(mParam1==1){
                Common.resultsAdapter = new ResultsRecycleViewAdapter(getActivity(), Common.filteredProposals_direct, isComplexSearch);
            }else if(mParam1==2){
                Common.resultsAdapter = new ResultsRecycleViewAdapter(getActivity(), Common.filteredProposals_best, isComplexSearch);
            }
        } else {
            if(mParam1==0) {
                Common.resultsAdapter.reloadFilteredTickets(Common.filteredProposals_all, SortUtils.getSavedSortingType());
            } else if(mParam1==1){
                Common.resultsAdapter.reloadFilteredTickets(Common.filteredProposals_direct, SortUtils.getSavedSortingType());
            }else if(mParam1==2){
                Common.resultsAdapter.reloadFilteredTickets(Common.filteredProposals_best, SortUtils.getSavedSortingType());
            }
        }
        adapter = Common.resultsAdapter;

        return adapter;
    }

    private SearchData getSearchResults() {
        return AviasalesSDK.getInstance().getSearchData();
    }

    private void showDetails(Proposal ticketData) {
        ProposalManager.getInstance().init(ticketData, AviasalesSDK.getInstance().getSearchData().getGatesInfo(),
                AviasalesSDK.getInstance().getSearchParamsOfLastSearch());
//        startFragment(TicketDetailsFragment.newInstance(), true);


        TicketDetailsFragment ticketDetailsFragment = TicketDetailsFragment.newInstance();
        ticketDetailsFragment.setIsPageFragment(true);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_ticket, ticketDetailsFragment, PageFragment_Direct.TAG)
                .addToBackStack(PageFragment_Direct.TAG)
                .commit();
    }



    private void checkAppDataAvailability() {
        if (getActivity() == null || getSearchResults() == null || getSearchParams() == null) {
            Toast.makeText(getActivity(), getResources().getString(R.string.toast_no_search_results), Toast.LENGTH_SHORT).show();
            getActivity().onBackPressed();
        } else {
            if (Common.resultsAdapter != null) {
                Common.resultsAdapter.notifyDataSetChanged();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();

//        if(mParam1==0) {
            checkAppDataAvailability();

            if (resultsCount != -1 && Common.resultsAdapter!=null && resultsCount != Common.resultsAdapter.getItemCount()) {
//                Common.resultsListView.scrollToPosition(0);
                resultsCount = -1;
            }
//        }
    }

    protected SearchParams getSearchParams() {
        return AviasalesSDK.getInstance().getSearchParamsOfLastSearch();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void resumeDialog(String removedDialogFragmentTag) {

    }

    @Override
    public void onPause() {
        super.onPause();

//        if(mParam1==0) {
            if (Common.resultsAdapter != null) {
                resultsCount = Common.resultsAdapter.getItemCount();
            }
//        }
    }

}
