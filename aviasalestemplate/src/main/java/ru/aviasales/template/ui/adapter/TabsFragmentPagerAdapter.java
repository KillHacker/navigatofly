package ru.aviasales.template.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ru.aviasales.template.R;
import ru.aviasales.template.ui.fragment.PageFragment;


/**
 * Created by Quan on 1/20/2017.
 */

public class TabsFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] { "ALL TICKETS", "DIRECT", "BEST" };
    private String tabSubCount[] = new String[] { "0", "0", "0" };
    private int intSubCount[] = new int[] { 0, 0, 0 };
    private Context context;


    public TabsFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;


        tabTitles[0] = context.getResources().getString(R.string.alltickets);
        tabTitles[1] = context.getResources().getString(R.string.direct);
        tabTitles[2] = context.getResources().getString(R.string.best);


    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public PageFragment getItem(int position) {
        PageFragment fragment;
//        if(position==1){
//            fragment = PageFragment_direct.newInstance(position, "param2");
//        }else {
            fragment = PageFragment.newInstance(position, "param2");
//        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(tabTitles[position]);
    }

}