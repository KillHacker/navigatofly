package ru.aviasales.template.ui.listener;

public interface OnDrawerMenuListener {
	void onSearch();
	void onMultiCity();
	void onPriceMap();
	void onSetting();
	void onLanguageCunrrency();
	void onShare();
	void onAbout();
}
