package ru.aviasales.template.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import ru.aviasales.core.search.params.SearchParams;
import ru.aviasales.core.search_airports.object.PlaceData;
import ru.aviasales.template.R;
import ru.aviasales.template.ads.AdsImplKeeper;
import ru.aviasales.template.currencies.Currency;
import ru.aviasales.template.ui.dialog.CurrencyFragmentDialog;
import ru.aviasales.template.ui.listener.AviasalesImpl;
import ru.aviasales.template.ui.listener.OnDrawerMenuListener;
import ru.aviasales.template.ui.model.SearchFormData;
import ru.aviasales.template.utils.Common;
import ru.aviasales.template.utils.CurrencyUtils;
import ru.aviasales.template.utils.StringUtils;
import ru.aviasales.template.utils.Utils;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class AviasalesFragment extends Fragment implements AviasalesImpl {

	public final static String TAG = "aviasales_fragment";
	public static boolean isMultiCity = false;

	public static TextView currencyTextView;

	private final static String TAG_CHILD = "aviasales_child_fragment";
	private static final String SEARCH_PARAMS_IS_UPDATED = "searh_params_is_updated";

	private final static int CACHE_SIZE = 20 * 1024 * 1024;
	private final static int CACHE_FILE_COUNT = 100;
	private final static int MEMORY_CACHE_SIZE = 5 * 1024 * 1024;
	private static final String AD_SHOWED_ONCE = "AD_SHOWED_ONCE";

	private FragmentManager fragmentManager;

	private SearchFormData searchFormData;

	private View rootView;
	private boolean adShowedOnce = false;

	public static Fragment newInstance() {
		return new AviasalesFragment();
	}

	public static Fragment newInstance(SearchFormData searchFormData)
	{
		AviasalesFragment aviasalesFragment = new AviasalesFragment();
		aviasalesFragment.searchFormData = searchFormData;
		return aviasalesFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null && savedInstanceState.containsKey(AD_SHOWED_ONCE)) {
			adShowedOnce = savedInstanceState.getBoolean(AD_SHOWED_ONCE);
		}

		migrateSearchFormTripClassParam();

		searchFormData = new SearchFormData(getActivity().getApplicationContext());
		initImageLoader(getActivity().getApplicationContext());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.aviasales_fragment_layout, container, false);
		if (!adShowedOnce) {
			adShowedOnce = true;
			rootView.postDelayed(new Runnable() {
				@Override
				public void run() {
					if (getActivity() != null) {
						AdsImplKeeper.getInstance().getAdsInterface().showStartAdsIfAvailable(getActivity());
					}
				}
			}, TimeUnit.SECONDS.toMillis(2));
		}
		return rootView;
	}

	private void migrateSearchFormTripClassParam() {
		SharedPreferences prefs = Utils.getPreferences(getActivity());

		if (!prefs.getBoolean(SEARCH_PARAMS_IS_UPDATED, false)) {
			prefs.edit().putBoolean(SEARCH_PARAMS_IS_UPDATED, true).apply();
			try {
				int tripClass = prefs.getInt(SearchParams.SEARCH_PARAM_TRIP_CLASS, -10);
				if (tripClass != -10) {
					prefs.edit().remove(SearchParams.SEARCH_PARAM_TRIP_CLASS).
							putString(SearchParams.SEARCH_PARAM_TRIP_CLASS, SearchParams.convertToNewTripClass(tripClass))
							.apply();
				}
			} catch (ClassCastException e) {
				//ignore class cast exception
			}
		}
	}

	public FragmentManager getAviasalesFragmentManager() {
		return fragmentManager;
	}

	public void startFragment(BaseFragment fragment, boolean shouldAddToBackStack) {

		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.replace(R.id.fragment_child_place, fragment, fragment.getClass().getSimpleName());
		if (shouldAddToBackStack) {
			fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		}
		fragmentTransaction.commit();

	}

	public void popFragmentFromBackStack() {
		onBackPressed();
	}


	private void initImageLoader(Context context) {
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.diskCacheSize(CACHE_SIZE)
				.diskCacheFileCount(CACHE_FILE_COUNT)
				.memoryCacheSize(MEMORY_CACHE_SIZE)
				.build();
		ImageLoader.getInstance().init(config);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		fragmentManager = this.getChildFragmentManager();

		SearchFormFragment fragment;
		if ((fragmentManager.findFragmentByTag(TAG_CHILD)) == null) {
			if(searchFormData != null)
				fragment = SearchFormFragment.newInstance(searchFormData);
			else
				fragment = SearchFormFragment.newInstance();
//			fragment.isMultiCity = isMultiCity;
			fragmentManager.beginTransaction().replace(R.id.fragment_child_place, fragment, TAG_CHILD).commit();
		}

	}

	private boolean mIsMultiCity = false;

	public void toSearchFormFragment(boolean isMultiCity){
		fragmentManager = this.getChildFragmentManager();

		SearchFormFragment fragment;
		if(searchFormData != null)
			fragment = SearchFormFragment.newInstance(searchFormData);
		else
			fragment = SearchFormFragment.newInstance();

		fragment.isMultiCity = isMultiCity;
		mIsMultiCity = isMultiCity;


		FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragment_child_place, fragment);
//        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());

//		fragmentTransaction.commit();
		fragmentTransaction.commitAllowingStateLoss();
	}

	public void showAboutFragment() {
		fragmentManager = this.getChildFragmentManager();

		AboutFragment fragment = new AboutFragment();

		FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragment_child_place, fragment);
//        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());

//		fragmentTransaction.commit();
		fragmentTransaction.commitAllowingStateLoss();
	}

	CurrencyFragmentDialog currencyDialog;
	public void createCurrencyDialog() {

		currencyDialog = CurrencyFragmentDialog.newInstance(new CurrencyFragmentDialog.OnCurrencyChangedListener() {
			@Override
			public void onCurrencyChanged(String code) {
				CurrencyUtils.setAppCurrency(code, getActivity());
				updateCurrencyTextView();
				if(Common.resultsListView!=null) {
					if (ResultsFragment.intSelectedTab == 0 && Common.resultsAdapter != null) {
						Common.resultsAdapter.notifyDataSetChanged();
						Common.resultsListView.setAdapter(Common.resultsAdapter);
					}
					if (ResultsFragment.intSelectedTab == 1 && ResultsFragment.proposalsAdapter_direct != null) {
						ResultsFragment.proposalsAdapter_direct.notifyDataSetChanged();
						Common.resultsListView.setAdapter(ResultsFragment.proposalsAdapter_direct);
					}
					if (ResultsFragment.intSelectedTab == 2 && ResultsFragment.proposalsAdapter_best != null) {
						ResultsFragment.proposalsAdapter_best.notifyDataSetChanged();
						Common.resultsListView.setAdapter(ResultsFragment.proposalsAdapter_best);
					}
				}
				currencyDialog.dismiss();
			}

			@Override
			public void onCancel() {
				currencyDialog.dismiss();
			}
		});
		List<Currency> currencies = CurrencyUtils.getCurrenciesList();
		currencyDialog.setItems(currencies);
		currencyDialog.show(fragmentManager,CurrencyFragmentDialog.TAG);
	}

	private void updateCurrencyTextView() {
		if(AviasalesFragment.currencyTextView!=null) {
			AviasalesFragment.currencyTextView.setText(StringUtils.getSpannableString(CurrencyUtils.getAppCurrency(getActivity()), new UnderlineSpan()));
		}
	}

	public boolean onBackPressed() {
		if (fragmentManager != null && fragmentManager.getBackStackEntryCount() > 0 && isVisible()) {
			fragmentManager.popBackStack();
			return true;
		} else {
			return false;
		}
	}

	public void popBackStackInclusive(String tag) {
		fragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	}

	@Override
	public void onStop() {
		searchFormData.saveState();
		super.onStop();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (rootView != null) {
			ViewGroup parentViewGroup = (ViewGroup) rootView.getParent();
			if (parentViewGroup != null) {
				parentViewGroup.removeAllViews();
			}
		}
	}

	@Override
	public void onAirportSelected(PlaceData placeData, int typeOfDate, int segment, boolean isComplex) {
		if (isComplex) {
			if (typeOfDate == SelectAirportFragment.TYPE_ORIGIN) {
				getSearchFormData().getComplexSearchSegments().get(segment).setOrigin(placeData);
			} else {
				getSearchFormData().getComplexSearchSegments().get(segment).setDestination(placeData);
			}
		} else {
			if (typeOfDate == SelectAirportFragment.TYPE_ORIGIN) {
				getSearchFormData().getSimpleSearchParams().setOrigin(placeData);
			} else {
				getSearchFormData().getSimpleSearchParams().setDestination(placeData);
			}
		}
	}

	@Override
	public SearchFormData getSearchFormData() {
		return searchFormData;
	}

	@Override
	public void saveState() {
		searchFormData.saveState();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putBoolean(AD_SHOWED_ONCE, adShowedOnce);
//		outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
		super.onSaveInstanceState(outState);
	}


	Locale myLocale;
	public void setLocale(String lang) {
		myLocale = new Locale(lang);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = myLocale;
		res.updateConfiguration(conf, dm);

//		Intent refresh = new Intent(getActivity(), getActivity().getClass());
//		startActivity(refresh);

		toSearchFormFragment(mIsMultiCity);


	}


}
